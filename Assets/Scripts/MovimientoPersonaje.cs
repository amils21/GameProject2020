﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour
{
    public float velocidad;
    Transform jugador;
    void Start()
    {
        jugador = GetComponent<Transform>();
    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal") * velocidad * Time.deltaTime;
        float vertical = Input.GetAxis("Vertical") * velocidad * Time.deltaTime;

        jugador.Translate(new Vector2(horizontal, vertical));
    }
}
