﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBala 
{
    void Disparar();
    void HacerDanio();
}
