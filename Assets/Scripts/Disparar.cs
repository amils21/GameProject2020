﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    Bala bala;
    // Start is called before the first frame update
    void Start()
    {
        bala = GetComponent<Bala>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("g"))
        {
            bala.SetStrategy(new BalaOscilante());
            bala.DispararBala();
        }
        if (Input.GetKeyDown("h"))
        {
            bala.SetStrategy(new BalaDivisible());
            bala.DispararBala();
        }
    }

}
