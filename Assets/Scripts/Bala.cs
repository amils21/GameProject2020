﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    private IBala _bala;
    public void SetStrategy(IBala bala)
    {
        _bala = bala;
    }
    public void DispararBala()
    {
        _bala.Disparar();
    }
    public void HacerDanio()
    {

    }
}
